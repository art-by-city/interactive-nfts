CREATE SCHEMA IF NOT EXISTS attendees;

DO $$ BEGIN
   CREATE TYPE attendees.request_state AS ENUM
     ('submitted', 'completed', 'rejected');
EXCEPTION
    WHEN duplicate_object THEN null;
END $$;

CREATE TABLE IF NOT EXISTS attendees.requests (
  id SERIAL,
  submitted_at timestamp with time zone NOT NULL DEFAULT now(),
  originator character(36) COLLATE pg_catalog."default" NOT NULL,
  request jsonb NOT NULL,
  state attendees.request_state NOT NULL DEFAULT 'submitted'::attendees.request_state,
  CONSTRAINT requests_pkey PRIMARY KEY (id)
)
