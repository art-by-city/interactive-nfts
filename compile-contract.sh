src=/home/jim/artbycity/interactive-nfts/smart-contracts/multi_asset/ligo/src/fa2_multi_asset.mligo
out=/home/jim/artbycity/interactive-nfts/smart-contracts/multi_asset/ligo/outjson/fa2_multi_asset.tz

docker run \
  --rm \
  -v /home/jim:/home/jim \
  -w /home/jim/ligo \
  ligolang/ligo:0.43.0 \
  compile \
  contract "$src" \
  --michelson-format json \
  --output-file "$out"
