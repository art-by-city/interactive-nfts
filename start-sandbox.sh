image=oxheadalpha/flextesa:latest
script=ithacabox
name=tz-sandbox

docker run \
  --rm \
  --name "$name" \
  --detach \
  -p 20000:20000 \
  -e block_time=3  \
  "$image" \
  "$script" \
  start

docker exec $name $script info
