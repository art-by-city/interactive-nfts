docker run \
  --name postgres \
  -e POSTGRES_USER=peppermint \
  -e POSTGRES_PASSWORD=foobar \
  -p 5432:5432 \
  -d \
  postgres
