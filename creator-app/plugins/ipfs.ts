import { Context } from '@nuxt/types'
import { Inject } from '@nuxt/types/app'
import { IPFSHTTPClient, create as createIPFS } from 'ipfs-http-client'

declare module 'vue/types/vue' {
  interface Vue {
    $ipfs: IPFSHTTPClient
  }
}

declare module '@nuxt/types' {
  interface NuxtAppOptions {
    $ipfs: IPFSHTTPClient
  }

  interface Context {
    $ipfs: IPFSHTTPClient
  }
}

declare module 'vuex/types/index' {
  interface Store<S> {
    $ipfs: IPFSHTTPClient
  }
}

export default async ({ $config }: Context, inject: Inject) => {
  const url = `${$config.baseUrl}/ipfs/api/v0`
  const ipfs = createIPFS({ url })
  inject('ipfs', ipfs)
}
