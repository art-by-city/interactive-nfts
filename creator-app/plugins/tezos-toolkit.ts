import { Context } from '@nuxt/types'
import { Inject } from '@nuxt/types/app'
import { TezosToolkit } from '@taquito/taquito'

declare module 'vue/types/vue' {
  interface Vue {
    $tezos: TezosToolkit
  }
}

declare module '@nuxt/types' {
  interface NuxtAppOptions {
    $tezos: TezosToolkit
  }

  interface Context {
    $tezos: TezosToolkit
  }
}

declare module 'vuex/types/index' {
  interface Store<S> {
    $tezos: TezosToolkit
  }
}

export default ({ $config }: Context, inject: Inject) => {
  inject('tezos', new TezosToolkit($config.tezos.rpc))
}
