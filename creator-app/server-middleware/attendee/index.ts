import Koa from 'koa'
import Router from '@koa/router'
import json from 'koa-json'
import bodyParser from 'koa-bodyparser'
import { Extension, TezosToolkit } from '@taquito/taquito'
import { Tzip12Module } from '@taquito/tzip12'
import {
  bytes2Char,
  getPkhfromPk,
  validateSignature,
  verifySignature,
  ValidationResult
} from '@taquito/utils'
import { Pool } from 'pg'
import { get } from 'http'

const contractLabel = process.env.CONTRACT_LABEL || 'interactive_nft_test'
const contractId = process.env.CONTRACT_ID
  || 'KT1VP9tp5CxFHLfnTUuWL58ttHGvmNcXQbkq'
const rpcUrl = process.env.RPC_URL || 'https://jakartanet.ecadinfra.com'

const app = new Koa()
const router = new Router()
const db = new Pool({
	user: process.env.POSTGRES_USER || 'peppermint',
	password: process.env.POSTGRES_PASSWORD || 'foobar',
	host: 'localhost',
	port: 5432,
	database: process.env.POSTGRES_DATABASE || 'peppermint'
})
const tezos = new TezosToolkit(rpcUrl)
const tzip12 = new Tzip12Module()
// NB: Why are these types incompatible when Tzip12Module implements Extension?
tezos.addExtension(tzip12 as unknown as Extension)

const INSERT_ATTENDEE_REQUEST_SQL = `
  INSERT INTO attendees.requests (originator, request)
  VALUES ($1, $2)
`
const SELECT_ALL_SUBMITTED_ATTENDEE_REQUESTS_SQL = `
  SELECT id, submitted_at, originator, request, state
  FROM attendees.requests
  WHERE state = 'submitted'
  ORDER BY submitted_at DESC
`

const SELECT_REQUEST_BY_ID_SQL = `
  SELECT id, submitted_at, originator, request, state
  FROM attendees.requests
  WHERE id = $1
  ORDER BY submitted_at DESC
`

const SELECT_REQUEST_BY_ATTENDEE_SQL = `
  SELECT id, submitted_at, originator, request, state
  FROM attendees.requests
  WHERE originator = $1
  ORDER BY submitted_at DESC
`

const SELECT_RECENT_MINTS_SQL = `
  SELECT *
  FROM ${contractLabel}.storage
  ORDER BY metadata_next_token_id DESC
  LIMIT 10
`

const signatureMiddleware = async (ctx: any, next: any) => {
  const { message, sig, pk } = ctx.request.body

  let isCreator = false
  try {
    const validationResult = validateSignature(sig)
    const isValid = ValidationResult.VALID === validationResult

    isCreator = isValid && verifySignature(message, pk, sig)
  } catch (err) {}

  if (isCreator) {
    return await next()
  } else {
    ctx.status = 401

    return
  }
}

router.post('/publish', signatureMiddleware, async (ctx, next) => {
  const { message, pk } = ctx.request.body
  const messageChars = bytes2Char(message)
  const publication = JSON.parse(
    messageChars.substring(messageChars.split(' ', 5).join(' ').length + 1)
  )
  const address = getPkhfromPk(pk)

  console.log('[ATTENDEE API] /publish', address, publication)

  const dbResult = await db.query(
    INSERT_ATTENDEE_REQUEST_SQL,
    [address, JSON.stringify(publication)]
  )

  console.log('[ATTENDEE API] /publish dbResult.rowCount', dbResult.rowCount)

  ctx.status = 200
  ctx.body = 'OK'
  await next()
})

router.get('/requests', async (ctx, next) => {
  const dbResult = await db.query(SELECT_ALL_SUBMITTED_ATTENDEE_REQUESTS_SQL)

  console.log('[ATTENDEE API] /requests dbResult.rowCount', dbResult.rowCount)

  ctx.status = 200
  ctx.body = dbResult.rows
  await next()
})

router.get('/request/:id', async (ctx, next) => {
  const dbResult = await db.query(SELECT_REQUEST_BY_ID_SQL, [ctx.params.id])

  console.log(
    `[ATTENDEE API] /request/${ctx.params.id} dbResult.rowCount`,
    dbResult.rowCount
  )

  ctx.status = 200
  ctx.body = dbResult.rows
  await next()
})

router.get('/attendee-requests/:address', async (ctx, next) => {
  const dbResult = await db.query(
    SELECT_REQUEST_BY_ATTENDEE_SQL,
    [ ctx.params.address ]
  )

  console.log(
    `[ATTENDEE API] /attendee-requests/${ctx.params.address} dbResult.rowCount`,
    dbResult.rowCount
  )

  ctx.status = 200
  ctx.body = dbResult.rows
  await next()
})

router.get('/recent-mints', async (ctx, next) => {
  const dbResult = await db.query(SELECT_RECENT_MINTS_SQL)

  console.log(
    `[ATTENDEE API] /recent-mints dbResult.rowCount`,
    dbResult.rowCount
  )

  const contract = await tezos.contract.at(contractId)
  console.log('[ATTENDEE API] /recent-mints got contract', contractId)
  const storage = await contract.storage() as any
  console.log('[ATTENDEE API] /recent-mints got contract storage')

  const mints = await Promise.all(
    dbResult.rows
      .map(row => {
        return Number.parseInt(row.metadata_next_token_id) - 1
      })
      .filter(token_id => {
        return token_id !== 0
      })
      .map(async token_id => {
        console.log(
          `[ATTENDEE API] /recent-mints fetching metadata for token id`,
          token_id
        )
        const metadata = await storage.assets.metadata.metadata.get({
          from_: token_id,
          to_: token_id + 1
        })
        console.log(
          `[ATTENDEE API] /recent-mints got metadata for token id`,
          token_id
        )
        const token_info = bytes2Char(await metadata.token_info.get(''))
        console.log(
          `[ATTENDEE API] /recent-mints got token_info for token id`,
          token_id,
          token_info
        )
        const cid = token_info.substring(7)
        console.log(
          `[ATTENDEE API] /recent-mints CID`,
          cid
        )
        const manifest = await new Promise((resolve, reject) => {
          get(`http://localhost:8080/ipfs/${cid}`, res => {
            let body = ''

            res.on('data', chunk => body += chunk)

            res.on('end', () => {
              try {
                console.log(
                  '[ATTENDEE API] /recent-mints got manifest',
                  body.length
                )
                resolve(JSON.parse(body))
              } catch (error) {
                reject(error)
              }
            })
          })
        })


        return { token_id, token_info, manifest }
      })
  )

  ctx.status = 200
  ctx.body = mints
  await next()
})

app
  .use(json())
  .use(bodyParser())
  .use(router.routes())
  .use(router.allowedMethods())

export default app.callback()
