import Koa from 'koa'
import Router from '@koa/router'
import json from 'koa-json'
import bodyParser from 'koa-bodyparser'
import {
  bytes2Char,
  getPkhfromPk,
  validateSignature,
  verifySignature,
  ValidationResult
} from '@taquito/utils'
import { create as createIPFS } from 'ipfs-http-client'
import { Pool } from 'pg'

const baseUrl = process.env.BASE_URL || 'http://localhost:3000'
const eventName = process.env.EVENT_NAME || 'Art By City - Tezos WAC Demo'
const ipfsUrl = process.env.IPFS_URL || 'http://localhost:5001'
const creatorPK = process.env.CREATOR_PK
  || 'edpkvGfYw3LyB1UcCahKQk4rF2tvbMUk8GFiTuMjL75uGXrpvKXhjn'
const creatorPKH = getPkhfromPk(creatorPK)

const app = new Koa()
const router = new Router()
const ipfs = createIPFS({ url: ipfsUrl })
const db = new Pool({
	user: process.env.POSTGRES_USER || 'peppermint',
	password: process.env.POSTGRES_PASSWORD || 'foobar',
	host: 'localhost',
	port: 5432,
	database: process.env.POSTGRES_DATABASE || 'peppermint'
})

const INSERT_MINT_REQUEST_SQL = `
  INSERT INTO peppermint.operations (originator, command)
  VALUES ($1, $2)
`

const GET_MAX_TOKEN_ID_SQL = `
  SELECT MAX(command->'args'->>'token_id')
  FROM peppermint.operations
`

const UPDATE_REQUEST_SUBMITTED_SQL = `
  UPDATE attendees.requests
  SET state = $2
  WHERE id = $1
`

const manifestBase = {
  decimals: 0,
  isBooleanAmount: true,
  minter: creatorPKH,
  type: 'Artwork',
  language: 'en',
  externalUri: baseUrl
}

router.post('/mint', async (ctx, next) => {
  const { message } = ctx.request.body
  const messageChars = bytes2Char(message)
  const publication = JSON.parse(
    messageChars.substring(messageChars.split(' ', 5).join(' ').length + 1)
  )

  console.log('[CREATOR API] Got mint request', publication)

  const manifest = {
    ...manifestBase,
    name: publication.title,
    description: publication.description,
    creators: [ creatorPKH, publication.attendee ],
    date: new Date().toISOString(),
    tags: [ eventName, 'tezos', 'artwork', 'nft', 'collab', 'Art By City' ],
    artifactUri: publication.url,
    displayUri: publication.url,
    thumbnailUri: publication.url
  }

  const { cid } = await ipfs.add(JSON.stringify(manifest))
  const manifestUri = `ipfs://${cid}`

  console.log('[CREATOR API] Manifest IPFS URI', manifestUri)

  let token_id = 1
  const lastTokenIdResult = await db.query(GET_MAX_TOKEN_ID_SQL)
  if (lastTokenIdResult.rowCount > 0) {
    const max = JSON.parse(lastTokenIdResult.rows[0].max)
    token_id = max
      ? max + 1
      : 1
  }

  console.log('[CREATOR API] /mint token_id', token_id)

  const dbResult = await db.query(INSERT_MINT_REQUEST_SQL, [
    creatorPKH,
    JSON.stringify({
      handler: 'nft-asset',
      name: 'mint_tokens',
      args: {
        token_id,
        metadata_uri: manifestUri,
        owners: [ publication.attendee ]
      }
    })
  ])

  console.log('[CREATOR API] /mint dbResult.rowCount', dbResult.rowCount)

  const updateStatusDbResult = await db.query(UPDATE_REQUEST_SUBMITTED_SQL, [
    publication.id,
    'completed'
  ])

  console.log(
    '[CREATOR API] /mint updateStatusDbResult.rowCount',
    updateStatusDbResult.rowCount
  )

  ctx.status = 200
  ctx.body = 'OK'
  await next()
})

app
  .use(json())
  .use(bodyParser())
  .use(async (ctx, next) => {
    const { message, sig } = ctx.request.body

    let isCreator = false
    try {
      const validationResult = validateSignature(sig)
      const isValid = ValidationResult.VALID === validationResult

      isCreator = isValid && verifySignature(message, creatorPK, sig)
    } catch (err) {}

    if (isCreator) {
      return await next()
    } else {
      ctx.status = 401

      return
    }
  })
  .use(router.routes())
  .use(router.allowedMethods())

export default app.callback()
