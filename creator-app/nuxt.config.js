import colors from 'vuetify/es5/util/colors'
import { NetworkType } from '@airgap/beacon-types'

const eventName = process.env.EVENT_NAME || 'Art By City - Tezos WAC Demo'
const baseUrl = process.env.BASE_URL || 'http://localhost:3000'
const network = process.env.TEZOS_NETWORK || NetworkType.JAKARTANET
const creator = process.env.CREATOR_ADDRESS
  || 'tz1VSUr8wwNhLAzempoch5d6hLRiTh8Cjcjb'
const ipfsUrl = process.env.IPFS_URL || 'http://localhost:5001'
const rpcUrl = process.env.RPC_URL || 'https://jakartanet.ecadinfra.com'
const host = baseUrl === 'http://localhost:3000'
  ? 'localhost'
  : '0'

export default {
  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    titleTemplate: `%s - ${eventName}`,
    title: eventName,
    htmlAttrs: {
      lang: 'en',
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' },
      { name: 'format-detection', content: 'telephone=no' },
    ],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }],
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    { src: '~/plugins/beacon-wallet.ts', ssr: false },
    { src: '~/plugins/tezos-toolkit.ts' },
    { src: '~/plugins/ipfs.ts' }
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // https://go.nuxtjs.dev/typescript
    '@nuxt/typescript-build',
    // https://go.nuxtjs.dev/vuetify
    '@nuxtjs/vuetify',
    'nuxt-typed-vuex'
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    // https://go.nuxtjs.dev/axios
    '@nuxtjs/axios',
  ],

  // Axios module configuration: https://go.nuxtjs.dev/config-axios
  axios: {
    // Workaround to avoid enforcing hard-coded localhost:3000: https://github.com/nuxt-community/axios-module/issues/308
    baseURL: '/',
    proxy: true
  },

  proxy: {
    '/gateway/': {
      target: 'http://localhost:8080/ipfs',
      pathRewrite: { '^/gateway/': '' },
      followRedirects: true
    },
    '/ipfs/': {
      target: ipfsUrl,
      pathRewrite: { '^/ipfs/': '' },
    }
  },

  serverMiddleware: [
    { path: '/creator-api', handler: '~/server-middleware/creator/index.ts' },
    { path: '/attendee-api', handler: '~/server-middleware/attendee/index.ts' }
  ],

  server: {
    host
  },

  // Vuetify module configuration: https://go.nuxtjs.dev/config-vuetify
  vuetify: {
    customVariables: ['~/assets/variables.scss'],
    theme: {
      dark: true,
      themes: {
        dark: {
          primary: colors.blue.darken2,
          accent: colors.grey.darken3,
          secondary: colors.amber.darken3,
          info: colors.teal.lighten1,
          warning: colors.amber.base,
          error: colors.deepOrange.accent4,
          success: colors.green.accent3,
        },
      },
    },
  },

  publicRuntimeConfig: {
    baseUrl,
    creator,
    eventName,
    dapp: {
      name: 'Art By City - Interactive NFTs',
      iconUrl: `${baseUrl}/favicon.ico`
    },
    tezos: {
      network,
      rpc: rpcUrl
    },
    ipfs: {
      url: ipfsUrl
    }
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {},
}
