endpoint=http://localhost:20000
pkh=tz1VSUr8wwNhLAzempoch5d6hLRiTh8Cjcjb
name=TESTCONTRACT
qty=0.0
prg=file:./smart-contracts/multi_asset/ligo/out/fa2_multi_asset.tz

# tezos-client \
#   --endpoint "$endpoint" \
#   get balance for "$pkh"

# Usable accounts:
# - alice
#   * edpkvGfYw3LyB1UcCahKQk4rF2tvbMUk8GFiTuMjL75uGXrpvKXhjn
#   * tz1VSUr8wwNhLAzempoch5d6hLRiTh8Cjcjb
#   * unencrypted:edsk3QoqBuvdamxouPhin7swCvkQNgq4jP5KZPbwWNnwdZpSpJiEbq
# - bob
#   * edpkurPsQ8eUApnLUJ9ZPDvu98E8VNj4KtJa1aZr16Cr5ow5VHKnz4
#   * tz1aSkwEot3L2kmUvcoxzjMomb9mvBNuzFK6
#   * unencrypted:edsk3RFfvaFaxbHx8BMtEW1rKQcPtDML3LXjNqMNLCzC3wLC1bWbAt


# docker exec tz-sandbox \
tezos-client \
  --endpoint "$endpoint" \
  originate contract "$name" \
  transferring "$qty" \
  from "$pkh" \
  running "$prg" -D
