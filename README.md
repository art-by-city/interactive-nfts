# Art By City Interactive NFT Framework

## Description
This project is an interactive NFT framework meant for cultural institutions holding artist events.
The code is broken into a few projects, all orchestrated with `docker-compose` for simplicity.
- `creator-app`
    - This is the main creator & attendee facing webapp, written in `TypeScript` with `NuxtJS`
    - The creator app includes a front-end and two back-end APIs: `attendee` & `creator`
    - Back-end APIs are protected with an authentication layer, based on Tezos wallet signatures
        - Requests that require authentication must include a message and signature signed by a Tezos wallet
        - Requests not including valid message and signature are rejected
        - This allows one webapp to be used securely by both attendees and the creator/artist
    - The front-end webapp is configured for an interactive NFT event where attendees are given a blank canvas
      to draw on, title, and describe.  The artist then receives these works and continues them, with ability
      to modify final title and description.
    - Both attendee and artist are considered to be the creator of the asset, while the attendee receives the NFT
    - The front-end webapp could easily be modified to support a number of interactive art exhibit use cases such
      as allowing an attendee to choose a pre-existing artwork from the artist for them to add their autograph to.
- `peppermint`
    - Batch minting engine
    - Forked from [tzConnectBerlin/peppermint](https://github.com/tzConnectBerlin/peppermint)
    - Added to this project as a git submodule.  Forked repository is here: [art-by-city/peppermint](https://github.com/art-by-city/peppermint)
- `que-pasa`
    - Tezos Contract Indexer
    - Official `Docker` image is used without modification
- `smart-contracts`
    - A collection of LIGO smart contracts
    - Forked from [oxheadalpha/smart-contracts](https://github.com/oxheadalpha/smart-contracts)
    - Added to this project as a git submodule.  Forked repository is here: [art-by-city/tezos-smart-contracts](https://gitlab.com/art-by-city/tezos-smart-contracts)
  
An `IPFS` node is included to support local asset uploads created by artists and attendees.

Data is stored in a `postgres` container.  This data (and the IPFS Node) should be persisted to a local disk by mounting a volume in `docker-compose.yml`

## Tezos WAC Hackathon Demo
Video demo at [https://youtu.be/31mnTJk9xYw](https://youtu.be/31mnTJk9xYw)

## Requirements
- `node` `16`
- `Docker`
- `Docker-Compose`
- a `Tezos` wallet, with a positive balance
- a deployed `Tezos` NFT Smart contract, see `Deploying Contracts` below.

## Environment Variables
Create an `.env` file in the root of this project and provide the following:
- `POSTGRES_USER` - your choice, defaults to `peppermint`
- `POSTGRES_PASSWORD` - your choice, defaults to `foobar`
- `POSTGRES_DATABASE` - your choice, defaults to `peppermint`
- `RPC_URL` - Tezos RPC Endpoint
- `TEZOS_NETWORK` - name of the target Tezos network, defaults to `jakartanet`
- `CREATOR_PK` - The public key of the creator/artist's Tezos Wallet.  Defaults to `alice` account from `flextesa`.
- `CREATOR_PRIVATE_KEY` - The private key of the creator/artist's Tezos Wallet, used by `Peppermint` for minting.  Defaults to `alice` account from `flextesa`.
- `BASE_URL` - If hosting under a custom domain, provide this here.  Otherwise, `localhost` will be used and will not work in production.
- `EVENT_NAME` - The name of the artist event or exhibit
- `NFT_NAME` - Name of the NFT Contract (used in contract metadata)
- `NFT_DESC` - Description of the NFT Contract (used in contract metadata)
- `CONTRACT_LABEL` - A label for this NFT Contract.  Used by `que-pasa` to create a schema in `postgres`.  Should be a valid `postgres` schema name (e.g. no spaces)
- `CONTRACT_ID` - After contract is deployed, provide the ID.  See `Deploying Contracts` below.

## Deploying Contracts
Before running, a target NFT contract should be deployed with the artist/creator's Tezos wallet
```bash
$ cd peppermint
$ npm i
$ node deploy-contract.mjs
```
The output will have the ID of the contract that should be added to your `.env` file, e.g.:
```Confirming origination for contract KT1EDZok4ghYGcJMtg7J7Y7Jmw5pD1o8PERf```

## Running
```bash
$ docker-compose up
```
Once ready, the webapp should be running at `http://localhost:3000`

## Rest API
### Authentication
All `POST` request body should have the following form:
```
{
  message: <string>,
  sig: <Signature of message>,
  pk: <Required only for Attendees, the Public Key of the Wallet used to generate the signature>
}
```
See `Taquito` docs for `message` format and how to sign requests: [generating-a-signature-with-beacon-sdk](https://tezostaquito.io/docs/signing#generating-a-signature-with-beacon-sdk).  The message itself is stringified JSON.

### Endpoints
- `POST /attendee-api/publish` Used by attendees to post their submissions
  - Message JSON
  ```
  {
    title: <Title of Artwork>,
    description: <Description of Artwork>,
    url: <IPFS URI of art asset>
  }
  ```

- `GET /attendee-api/requests` Fetch attendee submission requests in descending order of submission
  - Response Body
  ```
  [
    {
      id: <ID of submission>,
      submitted_at: <Timestamp of submission>,
      originator: <Attendee Public Key Hash (Tezos Address)>,
      request: {
        title: <Title of Artwork>,
        description: <Description of Artwork>,
        url: <IPFS URI of art asset>,
      }
    },
    ...
  ]
  ```

- `GET /attendee-api/request/:id` Fetch attendee submission request by ID
  - Response Body
  ```
  {
    id: <ID of submission>,
    submitted_at: <Timestamp of submission>,
    originator: <Attendee Public Key Hash (Tezos Address)>,
    request: {
      title: <Title of Artwork>,
      description: <Description of Artwork>,
      url: <IPFS URI of art asset>
    }
  }
  ```

- `GET /attendee-api/attendee-requests/:address` Fetch attendee requests by Tezos Wallet Address
  - Response Body
  ```
  [
    {
      id: <ID of submission>,
      submitted_at: <Timestamp of submission>,
      originator: <Attendee Public Key Hash (Tezos Address)>,
      request: {
        title: <Title of Artwork>,
        description: <Description of Artwork>,
        url: <IPFS URI of art asset>,
      }
    },
    ...
  ]
  ```

- `GET /attendee-api/recent-mints` Fetch recent NFT token mints
  - Response Body
  ```
  {
    token_id: number,
    token_info: <ipfs uri of metadata manifest>,
    manifest: <resolved metadata manifest>
  }
  ```

- `POST /creator-api/mint` Finalize an artwork and mint it
  - Message JSON
  ```
  {
    title: <Title of Artwork>,
    description: <Description of Artwork>,
    url: <IPFS URI of art asset>,
    attendee: <Tezos Address of Attendee>,
    id: <original request id>
  }
  ```

## Team
- Jim Toth, CEO <jim@artby.city>
- Slava, COO <slava@artby.city>
- Art By City DApp @ https://artby.city
- https://twitter.com/artbycity

## Incomplete / Further Work
- Allow creator/artist to reject submissions.
- Peppermint Batch Engine Updates
  - Some modifications were made to the Peppermint Batch Engine code to clean it up a bit and provide extended functionality.
  - This work remains in-place, incomplete, but with no detriment to core-functionality.
  - A `Driver` system was designed to be able to support multiple contracts simultaneously, as well as `Arweave` support
- `Arweave` Support for Peppermint Batch Engine
- Error handling
- Project cleanup
